# Instruction
1. Build dms-training-service-2 with Maven 3
2. Copy dms-training-service.war into path_to_wildfly_server/standalone/deployments
3. Start Wildfly
4. By using postman do a GET request
    http://localhost:8080/dms-training-service/V1/example/user/12
5. It should return,
    {
        "address": "Sri Lanka",
        "name": "DMS"
    }
