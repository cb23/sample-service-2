package com.dms.devops_traning.rest;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Describe what this API does.
 *
 */
@Stateless
@Path("/{versionID}/example/")
public class ExampleAPI {

	/**
	 * Retrieves user information by id.
	 * @param sc
	 * @param uriInfo
	 * @param headers
	 * @return
	 * @throws JsonProcessingException
	 */
	@GET
	@Path("/user/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserById(@Context SecurityContext sc, @Context UriInfo uriInfo, @Context HttpHeaders headers) throws JsonProcessingException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		Map<String,Object> returnObject = new HashMap<String, Object>();
		
		returnObject.put("name", "DMS Training Team");
		returnObject.put("address", "Sri Lanka");
		
		return Response.status(Response.Status.OK).entity(objectMapper.writeValueAsString(returnObject)).build();
	}
	
}
